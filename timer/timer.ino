
hw_timer_t *timer0_cfg = NULL;

const int ledPin = LED_BUILTIN;

void IRAM_ATTR onTimer() {
	digitalWrite(ledPin, !digitalRead(ledPin));
}

void setup() {
	pinMode(LED_BUILTIN, OUTPUT);

	timer0_cfg = timerBegin(0, 80, true);
	timerAttachInterrupt(timer0_cfg, &onTimer, true);
	timerAlarmWrite(timer0_cfg, 500000, true);
	timerAlarmEnable(timer0_cfg);
}

void loop() {
	/*digitalWrite(LED_BUILTIN, HIGH);
	delay(500);
	digitalWrite(LED_BUILTIN, LOW);
	delay(500);*/
}
