BOARD = esp32:esp32:firebeetle32
PORT = /dev/ttyUSB0

all: upload

compile:
	arduino-cli --warnings "-Werror -Wall -Wextra -Wshadow -Wundef -pedantic" compile -b ${BOARD} .

upload: compile
	arduino-cli upload -b ${BOARD} -p ${PORT} .

json:
	arduino-cli board attach -p ${PORT} .

clean:
	rm -f *.cfg debug_custom.json *.svd
