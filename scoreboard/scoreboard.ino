
#include <sstream>
#include <iomanip>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

#include "MyTimer.h"

#if defined __has_include
#if __has_include("credentials.h")
#include "credentials.h"
#else
const char *ssid = "<WIFI_SSID>";
const char *password = "<WIFI_PASSWORD";
#warning "Couldn't find credentials.h. Consider putting your info into a separate file."
#endif
#endif

#define PIN 12
#define mw 16
#define mh 16 

Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(16, 16, 1, 1, PIN,
                            NEO_MATRIX_TOP  + NEO_MATRIX_RIGHT +
                            NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG +
                            NEO_GRB         + NEO_KHZ800);


const uint16_t colors[] = {
  matrix.Color(255, 0, 0), matrix.Color(0, 255, 0), matrix.Color(0, 0, 255)
};

bool ledState = 0;
bool notifyState = false;
bool updateMatrix = false;
const int ledPin = LED_BUILTIN;

AsyncWebServer server(80);
AsyncWebSocket ws("/ws");

hw_timer_t *timer0_cfg = NULL;

// MyTimer watch(300); // 5 Minutes for testing
MyScoreboard scoreboard(10);

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <title>ESP Web Server</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,">
  <style>
  html {
    font-family: Arial, Helvetica, sans-serif;
    text-align: center;
  }
  h1 {
    font-size: 1.8rem;
    color: white;
  }
  h2{
    font-size: 1.5rem;
    font-weight: bold;
    color: #143642;
  }
  .topnav {
    overflow: hidden;
    background-color: #143642;
  }
  body {
    margin: 0;
  }
  .content {
    padding: 30px;
    max-width: 600px;
    margin: 0 auto;
  }
  .card {
    background-color: #F8F7F9;;
    box-shadow: 2px 2px 12px 1px rgba(140,140,140,.5);
    padding-top:10px;
    padding-bottom:20px;
  }
  .button {
    padding: 15px 50px;
    font-size: 24px;
    text-align: center;
    outline: none;
    color: #fff;
    background-color: #0f8b8d;
    border: none;
    border-radius: 5px;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: rgba(0,0,0,0);
   }
   /*.button:hover {background-color: #0f8b8d}*/
   .button:active {
     background-color: #0f8b8d;
     box-shadow: 2 2px #CDCDCD;
     transform: translateY(2px);
   }
   .state {
     font-size: 1.5rem;
     color:#8c8c8c;
     font-weight: bold;
   }
   #green {
     color:#008c00;
   }
   #blue {
     color:#00008c;
   }
   #separator {
     color:#8c008c;
   }
   #stoptime {
     font-size: 48px;
   }
  </style>
<title>ESP Web Server</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="data:,">
</head>
<body>
  <div class="topnav">
    <h1>Schubkarrenpolo Scoreboard</h1>
  </div>
  <div class="content">
    <div class="card">
      <h2>Spielzeit:</h2>
      <p class="state">
        <span id="stoptime">%STOPTIME%</span>
      </p>
      <p class="state"><span id="state" style="color:green;">%STATE%</span></p>
      <h2>Punktestand:</h2>
      <p class="state">
        <span id="green">%SGREEN%</span>
        <span id="separator">:</span> 
        <span id="blue">%SBLUE%</span>
      </p>
      <p>
        <button id="bpgreen" class="button">+</button>
        <button id="bmgreen" class="button">-</button>
        <button id="bpblue" class="button">+</button>
        <button id="bmblue" class="button">-</button>
      </p>
      <p><button id="button" class="button">Start/Stop</button></p>
      <p><button id="reset" class="button">Reset time</button></p>
    </div>
  </div>
<script>
  var gateway = `ws://${window.location.hostname}/ws`;
  var websocket;
  window.addEventListener('load', onLoad);
  function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen    = onOpen;
    websocket.onclose   = onClose;
    websocket.onmessage = onMessage; // <-- add this line
  }
  function onOpen(event) {
    console.log('Connection opened');
  }
  function onClose(event) {
    console.log('Connection closed');
    setTimeout(initWebSocket, 2000);
  }
  function onMessage(event) {
    var state;

    const obj = JSON.parse(event.data);

    console.log("Got: " + JSON.stringify(obj));
    if(obj.running == "1") {
      state = "ON";
    } else {
      state = "OFF";
    }

    document.getElementById('state').innerHTML = state;
    document.getElementById('stoptime').innerHTML = obj.timer;
    document.getElementById('green').innerHTML = obj.green;
    document.getElementById('blue').innerHTML = obj.blue;
  }
  function onLoad(event) {
    initWebSocket();
    initButton();
  }
  function initButton() {
    document.getElementById('button').addEventListener('click', toggle);
    document.getElementById('reset').addEventListener('click', reset);
    document.getElementById('bpgreen').addEventListener('click', bpgreen);
    document.getElementById('bmgreen').addEventListener('click', bmgreen);
    document.getElementById('bpblue').addEventListener('click', bpblue);
    document.getElementById('bmblue').addEventListener('click', bmblue);
  }
  function toggle(){
    websocket.send('toggle');
  }
  function reset(){
    websocket.send('reset');
  }
  function bpgreen() {
    websocket.send('bpgreen');
  }
  function bmgreen() {
    websocket.send('bmgreen');
  }
  function bpblue() {
    websocket.send('bpblue');
  }
  function bmblue() {
    websocket.send('bmblue');
  }
</script>
</body>
</html>
)rawliteral";

void notifyClients() {
	ws.textAll(String(scoreboard.toJson().c_str()));
}

void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
	AwsFrameInfo *info = (AwsFrameInfo *) arg;
	if(info->final && info->index == 0 && 
			info->len == len && info->opcode == WS_TEXT) {

		data[len] = 0;
		if(strcmp((char*)data, "toggle") == 0) {
      scoreboard.setRunning(!scoreboard.isRunning());
		} else if(strcmp((char*)data, "reset") == 0) {
      scoreboard.reset();
		} else if(strcmp((char*)data, "bpgreen") == 0) {
      scoreboard.setGreen(scoreboard.getGreen() + 1);
		} else if(strcmp((char*)data, "bmgreen") == 0) {
      scoreboard.setGreen(scoreboard.getGreen() == 0 ? 
          scoreboard.getGreen() : scoreboard.getGreen()-1);
		} else if(strcmp((char*)data, "bpblue") == 0) {
      scoreboard.setBlue(scoreboard.getBlue() + 1);
		} else if(strcmp((char*)data, "bmblue") == 0) {
      scoreboard.setBlue(scoreboard.getBlue() == 0 ? 
          scoreboard.getBlue() : scoreboard.getBlue()-1);
    }
    notifyClients();
	}
}

void onEvent(AsyncWebSocket *server, AsyncWebSocketClient *client, 
		AwsEventType type, void *arg, uint8_t *data, size_t len) {

	switch(type) {
		case WS_EVT_CONNECT:
			Serial.printf("Websocket client #%u connected from %s\r\n", client->id(), client->remoteIP().toString().c_str());
      notifyState = true;
			break;
		case WS_EVT_DISCONNECT:
			Serial.printf("Websocket client #%u disconnected\r\n", client->id());
			break;
		case WS_EVT_DATA:
			handleWebSocketMessage(arg, data, len);
			break;
		case WS_EVT_PONG:
		case WS_EVT_ERROR:
			break;
	}
}

void initWebSocket() {
	ws.onEvent(onEvent);
	server.addHandler(&ws);
}

String processor(const String& var){
  Serial.println(var);
  if(var == "STATE"){
    if (ledState){
      return "ON";
    }
    else{
      return "OFF";
    }
  } else if(var == "STOPTIME") {
    Serial.println(String(scoreboard.toString().c_str()));
    return String(scoreboard.toString().c_str());
  } else if(var == "SGREEN") {
    return String(scoreboard.getGreen());
  } else if(var == "SBLUE") {
    return String(scoreboard.getBlue());
  }
  return String();
}

void print_number(size_t x, size_t y, size_t n) {
  matrix.setCursor(x, y);
  matrix.print(String(n%10));
}

void print_minutes(size_t m) {
  matrix.setTextColor(colors[1]);
  print_number(7, 0, m % 10);
  m /= 10;
  print_number(1, 0, m % 10);
}

void print_seconds(size_t s) {
  matrix.setTextColor(colors[2]);
  print_number(7, 8, s % 10);
  s /= 10;
  print_number(1, 8, s % 10);
}

void print_time(size_t seconds) {
  matrix.fillScreen(0);
  print_minutes(seconds / 60);
  print_seconds(seconds % 60);
}

void draw_finish() {
  /* TODO: make this more pretty */
  const uint16_t clrs[] = {
    matrix.Color(0, 0, 0), matrix.Color(255, 255, 255)
  };
  for(auto n = 0; n < (mw*mh); n++) {
    matrix.setPixelColor(n, n%2 ? clrs[0] : clrs[1]);
  }
}

void IRAM_ATTR onTimer() {
	/*digitalWrite(ledPin, !digitalRead(ledPin));*/
  if(scoreboard.isRunning()) {
    scoreboard.tick();
    if(scoreboard.isFinished()) {
      scoreboard.setRunning(false);
    }
    ledState = !ledState;
    notifyState = true;
  }
  updateMatrix = true;
}

void setup() {
	Serial.begin(115200);

	pinMode(ledPin, OUTPUT);
	digitalWrite(ledPin, LOW);

  notifyState = false;

	delay(2000);

	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.println("Connecting to WiFi ...");
	}

	Serial.println(WiFi.localIP());
	initWebSocket();

	server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
		request->send_P(200, "text/html", index_html, processor);
	});

	timer0_cfg = timerBegin(0, 80, true);
	timerAttachInterrupt(timer0_cfg, &onTimer, true);
	timerAlarmWrite(timer0_cfg, 1000000, true);
	timerAlarmEnable(timer0_cfg);

	server.begin();

  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setBrightness(100);
  matrix.setTextColor(colors[1]);
  matrix.setTextSize(1);
}

void loop() {
	ws.cleanupClients();
  digitalWrite(ledPin, ledState);
  if(notifyState) {
    notifyClients();
    notifyState = false;
  }
  if(updateMatrix) {
    if(!scoreboard.isFinished())
      print_time(scoreboard.getSeconds());
    else
      draw_finish();
    matrix.show();
  }
}
