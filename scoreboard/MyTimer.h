#pragma once

#include <iostream>
#include <sstream>
#include <stdint.h>
#include <iomanip>

class MyTimer {
  public:
    MyTimer() // default with 5 minute timer
      : _max_seconds(300), _seconds(300), _running(false) {}
    MyTimer(uint32_t seconds) 
      : _max_seconds(seconds), _seconds(seconds), _running(false) {};

    [[nodiscard]] auto toJson() -> std::string {
      std::stringstream ss;

      ss << "{" 
        << "\"running\" : \"" << this->_running << "\"," 
        << "\"timer\" : \"" << this->_seconds/60 << ":" 
        << std::setfill('0') << std::setw(2) << this->_seconds%60 << "\"" 
        << "}";

      return ss.str();
    }

    [[nodiscard]] auto toString() -> std::string {
      std::stringstream ss;

      ss << this->_seconds/60 << ":" << this->_seconds%60;

      return ss.str();
    }

    void reset() { _seconds = _max_seconds; }
    void setMax(uint32_t s) { _max_seconds = s; }
    [[nodiscard]] auto getMax() -> uint32_t { return _max_seconds; }
    [[nodiscard]] auto getSeconds() -> uint32_t { return _seconds; }

    void setRunning(bool r) { _running = r; }
    [[nodiscard]] auto isRunning() -> bool { return _running; }

    void tick() { if(_seconds > 0) _seconds--; }
    [[nodiscard]] auto isFinished() -> bool { return (_seconds == 0); }
  
  protected:
    bool _running;
    uint32_t _max_seconds;
    uint32_t _seconds;
};

class MyScoreboard : public MyTimer {
  public:
    MyScoreboard() : MyTimer(), _green(0), _blue(0) {}
    MyScoreboard(uint32_t seconds) : MyTimer(seconds), _green(0), _blue(0) {}

    [[nodiscard]] auto toJson() -> std::string {
      std::stringstream ss;

      ss << "{" 
        << "\"running\" : \"" << this->_running << "\"," 
        << "\"timer\" : \""
        << std::setfill('0') << std::setw(2) << this->_seconds/60 << ":" 
        << std::setfill('0') << std::setw(2) << this->_seconds%60 << "\"," 
        << "\"green\" : \"" << this->_green << "\"," 
        << "\"blue\" : \"" << this->_blue << "\"" 
        << "}";

      return ss.str();
    }

    auto getGreen() -> uint32_t { return _green; }
    auto getBlue() -> uint32_t { return _blue; }
    void setGreen(uint32_t green) { _green = green; }
    void setBlue(uint32_t blue) { _blue = blue; }

  private:
    uint32_t _green;
    uint32_t _blue;

};
