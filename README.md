Some Arduino sketches.

## Info

If you don't want to place your WiFi credentials into the .ino file (in case
you want to version it with git), then just create a credentials.h in the given
project directory with the following content:

```c
#pragma once

const char *ssid = "<YOUR_SSID>";
const char *password = "<YOUR_PASSWORD>";
```

## Requirements

Install arduino-cli first ( https://arduino.github.io/arduino-cli/ )

Board and Port have to be adjusted in the common.mk file. Then just do

```
make compile
make upload
```
